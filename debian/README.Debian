Architecture support
====================

bin-prot works at its best on little endian architecture and has been
thoroughly tested only on i386 and amd64. Using it on other architectures can
exhibit a number of limitation. Here is a summary of such limitation taken from
a post by upstream author Markus Mottl to the Caml mailing list
(http://groups.google.com/group/fa.caml/msg/80da07bb322f2d7a)

> We currently do not have any immediate need and man power to fully
> support big-endian machines (we also don't have access to any), but
> we'll gladly accept patches.  This could be implemented using
> platform-specific macros as is the case with 32/64bit.
> 
> This is what works / doesn't work as of now:
> 
>   *) Big-endian and little-endian machines cannot communicate with
> each other for anything but very specific cases.  Don't use the binary
> protocol in such heterogeneous environments.
> 
>   *) 32 and 64 bit, little-endian architectures can communicate
> freely, assuming, of course, that integers do not overflow on 32bit.
> This is tested at runtime to prevent hard to debug errors.
> 
>   *) 32bit big-endian machines can communicate with each other freely.
> 64 bit big-endians can communicate with each other freely, too, but
> not necessarily with 32bit big-endians: values of type int64, etc.,
> may not necessarily be communicated correctly.  Note, too, that you
> should exclusively use the automatically generated converters on
> big-endian machines.  The hand-written (slower) ones for the basic
> types are intended mostly for testing purposes only, and will not work
> when mixed with a different endianness (they assume little-endians).
> 
> Thus, it certainly makes sense to package the binary protocol for
> big-endians, too, as long as people are informed of what works.  Since
> almost nobody uses big-endian machines, most users won't care.  But
> I'd surely be happy to see a patch to fully support all
> architectures... 

 -- Stéphane Glondu <glondu@debian.org>, Wed, 17 Aug 2016 13:52:48 +0200
